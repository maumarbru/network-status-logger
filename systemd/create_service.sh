SYSTEMD_SCRIPT_DIR=$( cd  $(dirname "${BASH_SOURCE:=$0}") && pwd)
cp -f "$SYSTEMD_SCRIPT_DIR/network-status-logger.service" /lib/systemd/system
chown root:root /lib/systemd/system/network-status-logger.service

systemctl daemon-reload
systemctl enable network-status-logger.service
systemctl restart network-status-logger.service
