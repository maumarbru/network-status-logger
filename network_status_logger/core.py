import os
import sys
import urllib.request
import urllib.error
import argparse
import time
from datetime import datetime

DATE_FORMAT = "%Y:%m:%d %H:%M:%S"


class Measurement:
    '''Measuremente description'''

    @classmethod
    def is_ok(cls):
        return False


class HTTPMeasurement(Measurement):

    timeout = 1
    url = None

    @classmethod
    def is_ok(cls):
        try:
            urllib.request.urlopen(cls.url, timeout=cls.timeout)
            return True
        except:
            return False


class GoogleHTTPMeasurement(HTTPMeasurement):
    '''HTTP www.google.com'''
    url = "http://www.google.com"


class PingMeasurement(Measurement):
    host = None

    @classmethod
    def is_ok(cls):
        cmd = "ping -c 1 {} > /dev/null 2> /dev/null".format(cls.host)
        error = os.system(cmd)
        return error == 0


class GoogleIPPingMeasurement(PingMeasurement):
    '''Ping Google 8.8.8.8'''
    host = "8.8.8.8"


class LocalhostPingMeasurement(PingMeasurement):
    '''Ping localhost'''
    host = "localhost"


class WhiteboxPingMeasurement(PingMeasurement):
    '''Ping Whitebox'''
    host = "192.168.2.1"


measurements = [
    LocalhostPingMeasurement,
    WhiteboxPingMeasurement,
    GoogleIPPingMeasurement,
    GoogleHTTPMeasurement,
]


def main(args=None):
    args = args or sys.argv[1:]
    parser = argparse.ArgumentParser()
    parser.add_argument('--interval-seconds', default=1, type=float)
    parser.add_argument('--no-log-timestamp', action='store_true')
    args = parser.parse_args(args)
    loop(args.interval_seconds, not args.no_log_timestamp)


def loop(interval_seconds, log_timestamp=True):
    try:
        while True:
            for measurement in measurements:
                status = 'OK' if measurement.is_ok() else 'Error'
                if log_timestamp:
                    timestamp = time.time()
                    timestamp_string = datetime.fromtimestamp(timestamp).strftime(DATE_FORMAT)
                    msg = '{}: {:20} [{}]'.format(timestamp_string, measurement.__doc__, status)
                else:
                    msg = '{:20} [{}]'.format(measurement.__doc__, status)
                print(msg, flush=True)
            time.sleep(interval_seconds)
    except KeyboardInterrupt:
        pass
