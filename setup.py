from setuptools.command.install import install
from pip.req import parse_requirements
from setuptools import setup, find_packages
from glob import glob
import subprocess
import os

install_reqs = parse_requirements('./requirements.txt', session=False)
reqs = [str(ir.req) for ir in install_reqs]


class CustomInstallCommand(install):

    def run(self):
        install.run(self)
        current_dir_path = os.path.dirname(os.path.realpath(__file__))
        create_service_script_path = os.path.join(
            current_dir_path, 'systemd', 'create_service.sh')
        subprocess.check_output(create_service_script_path, shell=True)

setup(name='network-status-logger',
      version='0.0.1',
      description='Network status logger',
      author='Mauro Bruni',
      author_email='maumarbru@gmail.com',
      packages=find_packages(exclude=["tests"]),
      license="?",
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Programming Language :: Python',
          'Topic :: Software Development :: Libraries :: Python Modules',
          'Topic :: Utilities',
      ],
      install_requires=reqs,
      test_suite="tests",
      entry_points={
          'console_scripts': [
              'network-status-logger = network_status_logger.core:main',
          ]
      },
      cmdclass={'install': CustomInstallCommand},
      )
